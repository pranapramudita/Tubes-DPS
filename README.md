# Tubes-DPS Topik Games Of Craps
Craps is a dice game in which the players make wagers on the outcome of the roll, or a series of rolls, of a pair of dice. Players may wager money against each other (playing "street craps") or a bank (playing "casino craps", also known as "table craps", or often just "craps"). Because it requires little equipment, "street craps" can be played in informal settings. While shooting craps, players may use slang terminology to place bets and actions.

Steps :
1. 2 dadu dilempar trus di sum
2. menang if sum 7 or 11, kalah if sum 2, 3 or 12
3. selain itu, simpan nilai, repeat sampai ketemu nilai yang sama lagi baru win

reference's video : 
1. https://www.youtube.com/watch?v=tKzFb84G7oY (Probability)
2. https://www.youtube.com/watch?v=7Vom4YWEOI0 (Basic Rule of Games of Craps)
